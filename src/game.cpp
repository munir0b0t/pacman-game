#include <vector>
#include <iostream>
#include <fstream>

#include <Eigen/Core>

#include "game.h"

State game_state = W;
const unsigned int game_speed = 4;
unsigned int tick = 0, tock = 0;

char **tile_map;
int map_width, map_height;
float tile_width, tile_height;

Eigen::Matrix<float, 5, 4> right_eye, left_eye;
float eye_tex_t, eye_tex_b, eye_tex_l, eye_tex_r;

char **load_tile_map(const char *fname, int& width, int& height)
{
	using namespace std;

	char **map = NULL;
	string line;
	ifstream infile(fname);

	getline(infile, line);
	istringstream iss(line);
	iss >> width >> height;

	map = (char **) calloc(height, sizeof *map);
	for (unsigned i = 0; i < height; i++)
		map[i] = (char *) calloc(width, sizeof *map[i]);

	for (unsigned i = 0; i < height; i++) {
		getline(infile, line);
		for (unsigned j = 0; j < width; j++)
			map[i][j] = line[j];
	}
	return map;
}

void free_tile_map(char **map, int width, int height)
{
	for (unsigned i = 0; i < height; i++)
		free(map[i]);
	free(map);
}

bool hits_wall(const Sprite& sprite, const Direction direction)
{
	int x = int(sprite.centre(0));
	int y = int(sprite.centre(1));

	if (float(x) != sprite.centre(0))
		return (direction == U || direction == D);
	if (float(y) != sprite.centre(1))
		return (direction == L || direction == R);

	switch (direction) {
	case U:
		return (y != 0)
			&& (not_walls.find(tile_map[y - 1][x]) == std::string::npos);
	case D:
		return (y != (map_height - 1))
			&& (not_walls.find(tile_map[y + 1][x]) == std::string::npos);
	case L:
		return (x != 0)
			&& (not_walls.find(tile_map[y][x - 1]) == std::string::npos);
	case R:
		return (x != (map_width - 1))
			&& (not_walls.find(tile_map[y][x + 1]) == std::string::npos);
	case X:
	default:
		return true;
	}
}

void Sprite::initialize(int tile_x, int tile_y, float tex_x, float tex_y, float tex_width, float tex_height)
{
	init_position << float(tile_x), float(tile_y);

	matrix << Eigen::MatrixXf::Zero(5, 26);

	matrix.col(0) << 0.0, 0.0, 0.0, tex_x, tex_y;
	matrix.col(1) << -0.5, 0.25, 0.0, tex_x, tex_y;
	matrix.col(2) << -0.5, 0.5, 0.0, tex_x, tex_y;
	matrix.col(3) << -0.25, 0.5, 0.0, tex_x, tex_y;
	matrix.col(4) << 0.0, 0.5, 0.0, tex_x, tex_y;
	matrix.col(5) << 0.25, 0.5, 0.0, tex_x, tex_y;
	matrix.col(6) << 0.5, 0.5, 0.0, tex_x, tex_y;
	matrix.col(7) << 0.5, 0.25, 0.0, tex_x, tex_y;
	matrix.col(8) << 0.5, 0.0, 0.0, tex_x, tex_y;
	matrix.col(9) << 0.5, -0.25, 0.0, tex_x, tex_y;
	matrix.col(10) << 0.5, -0.5, 0.0, tex_x, tex_y;
	matrix.col(11) << 0.25, -0.5, 0.0, tex_x, tex_y;
	matrix.col(12) << 0.0, -0.5, 0.0, tex_x, tex_y;
	matrix.col(13) << -0.25, -0.5, 0.0, tex_x, tex_y;
	matrix.col(14) << -0.5, -0.5, 0.0, tex_x, tex_y;
	matrix.col(15) << -0.5, -0.25, 0.0, tex_x, tex_y;
	matrix.col(16) << -0.5, 0.0, 0.0, tex_x, tex_y;
	matrix.col(17) << -0.5, 0.25, 0.0, tex_x, tex_y;

	matrix.row(3) += (tex_width * matrix.row(0));
	matrix.row(4) -= (tex_height * matrix.row(1));

	matrix.row(0) *= tile_width;
	matrix.row(1) *= tile_height;

	for (unsigned i = 0; i < 18; i++) {
		if (matrix(0, i) < 0.0)
			matrix(0, i) -= (0.33 * tile_width);
		else if (matrix(0, i) > 0.0)
			matrix(0, i) += (0.33 * tile_width);

		if (matrix(1, i) < 0.0)
			matrix(1, i) -= (0.25 * tile_height);
		else if (matrix(1, i) > 0.0)
			matrix(1, i) += (0.25 * tile_height);
	}

	reset();
}

void Sprite::reset(void)
{
	centre << init_position(0) - 0.5, init_position(1);

	transform << Eigen::Matrix4f::Identity(4, 4);
	transform(0, 3) = get_xpos_from_tile(init_position(0));
	transform(1, 3) = get_ypos_from_tile(init_position(1) + 0.5);
	transform(2, 3) = -0.1;

	eye_transform << Eigen::Matrix4f::Identity(4, 4);
	eye_transform(0, 3) = get_xpos_from_tile(init_position(0));
	eye_transform(1, 3) = get_ypos_from_tile(init_position(1) + 0.5);
	eye_transform(2, 3) = -0.1;

	direction = X;
	next_direction = X;
	last_kill_tock = cool_down_tock + tock;
}

void Sprite::update_direction(Direction new_direction)
{
	switch (new_direction) {
	case U:
		if (rotate)
			transform.block<2, 2>(0, 0) << 0, 1, -1, 0;
		if (has_eyes) {
			matrix.block<2, 1>(3, 18) << eye_tex_r, eye_tex_b;
			matrix.block<2, 1>(3, 19) << eye_tex_r, eye_tex_t;
			matrix.block<2, 1>(3, 20) << eye_tex_l, eye_tex_b;
			matrix.block<2, 1>(3, 21) << eye_tex_l, eye_tex_t;
			matrix.block<2, 1>(3, 22) << eye_tex_r, eye_tex_b;
			matrix.block<2, 1>(3, 23) << eye_tex_r, eye_tex_t;
			matrix.block<2, 1>(3, 24) << eye_tex_l, eye_tex_b;
			matrix.block<2, 1>(3, 25) << eye_tex_l, eye_tex_t;
		}

		transform(1, 3) += (tile_height * 0.5);
		centre(1) -= 0.5;

		if (centre(1) < 0.0)
			centre(1) += map_height;
		if (transform(1, 3) >= (tile_height * map_height * 0.5))
			transform(1, 3) -= (tile_height * map_height);

		direction = new_direction;
		break;
	case D:
		if (rotate)
			transform.block<2, 2>(0, 0) << 0, -1, 1, 0;
		if (has_eyes) {
			matrix.block<2, 1>(3, 18) << eye_tex_l, eye_tex_t;
			matrix.block<2, 1>(3, 19) << eye_tex_l, eye_tex_b;
			matrix.block<2, 1>(3, 20) << eye_tex_r, eye_tex_t;
			matrix.block<2, 1>(3, 21) << eye_tex_r, eye_tex_b;
			matrix.block<2, 1>(3, 22) << eye_tex_l, eye_tex_t;
			matrix.block<2, 1>(3, 23) << eye_tex_l, eye_tex_b;
			matrix.block<2, 1>(3, 24) << eye_tex_r, eye_tex_t;
			matrix.block<2, 1>(3, 25) << eye_tex_r, eye_tex_b;
		}

		transform(1, 3) -= (tile_height * 0.5);
		centre(1) += 0.5;

		if (centre(1) >= map_height)
			centre(1) -= map_height;
		if (transform(1, 3) < (tile_height * map_height * -0.5))
			transform(1, 3) += (tile_height * map_height);

		direction = new_direction;
		break;
	case L:
		if (rotate)
			transform.block<2, 2>(0, 0) << 1, 0, 0, 1;
		if (has_eyes) {
			matrix.block<2, 1>(3, 18) << eye_tex_l, eye_tex_b;
			matrix.block<2, 1>(3, 19) << eye_tex_r, eye_tex_b;
			matrix.block<2, 1>(3, 20) << eye_tex_l, eye_tex_t;
			matrix.block<2, 1>(3, 21) << eye_tex_r, eye_tex_t;
			matrix.block<2, 1>(3, 22) << eye_tex_l, eye_tex_b;
			matrix.block<2, 1>(3, 23) << eye_tex_r, eye_tex_b;
			matrix.block<2, 1>(3, 24) << eye_tex_l, eye_tex_t;
			matrix.block<2, 1>(3, 25) << eye_tex_r, eye_tex_t;
		}

		transform(0, 3) -= (tile_width * 0.5);
		centre(0) -= 0.5;

		if (centre(0) < 0.0)
			centre(0) += map_width;
		if (transform(0, 3) < (tile_width * map_width * -0.5))
			transform(0, 3) += (tile_width * map_width);

		direction = new_direction;
		break;
	case R:
		if (rotate)
			transform.block<2, 2>(0, 0) << -1, 0, 0, -1;
		if (has_eyes) {
			matrix.block<2, 1>(3, 18) << eye_tex_r, eye_tex_t;
			matrix.block<2, 1>(3, 19) << eye_tex_l, eye_tex_t;
			matrix.block<2, 1>(3, 20) << eye_tex_r, eye_tex_b;
			matrix.block<2, 1>(3, 21) << eye_tex_l, eye_tex_b;
			matrix.block<2, 1>(3, 22) << eye_tex_r, eye_tex_t;
			matrix.block<2, 1>(3, 23) << eye_tex_l, eye_tex_t;
			matrix.block<2, 1>(3, 24) << eye_tex_r, eye_tex_b;
			matrix.block<2, 1>(3, 25) << eye_tex_l, eye_tex_b;
		}

		transform(0, 3) += (tile_width * 0.5);
		centre(0) += 0.5;

		if (centre(0) >= map_width)
			centre(0) -= map_width;
		if (transform(0, 3) >= (tile_width * map_width * 0.5))
			transform(0, 3) -= (tile_width * map_width);

		direction = new_direction;
		break;
	case X:
	default:
		direction = new_direction;
		break;
	}

	eye_transform.block<2, 1>(0, 3) << transform.block<2, 1>(0, 3);
}

void Sprite::update(Direction new_direction)
{
	if (!hits_wall(*this, new_direction))
		update_direction(new_direction);
	else if (!hits_wall(*this, direction))
		update_direction(direction);
	else
		update_direction(X);
}

void Sprite::add_eyes(float tex_x, float tex_y, float tex_width, float tex_height)
{
	left_eye.col(0) << -0.65, 0.60, -0.1, tex_x, tex_y;
	left_eye.col(1) << -0.65, -0.20, -0.1, tex_x, tex_y + tex_height;
	left_eye.col(2) << -0.05, 0.60, -0.1, tex_x + tex_width, tex_y;
	left_eye.col(3) << -0.05, -0.20, -0.1, tex_x + tex_width, tex_y + tex_height;

	right_eye << left_eye;
	right_eye.row(0) += Eigen::Vector4f(0.70, 0.70, 0.70, 0.70);

	left_eye.row(0) *= tile_width;
	left_eye.row(1) *= tile_height;

	right_eye.row(0) *= tile_width;
	right_eye.row(1) *= tile_height;

	matrix.col(18) << left_eye.col(0);
	matrix.col(19) << left_eye.col(1);
	matrix.col(20) << left_eye.col(2);
	matrix.col(21) << left_eye.col(3);
	matrix.col(22) << right_eye.col(0);
	matrix.col(23) << right_eye.col(1);
	matrix.col(24) << right_eye.col(2);
	matrix.col(25) << right_eye.col(3);

	has_eyes = true;

	eye_tex_l = tex_x;
	eye_tex_r = tex_x + tex_width;
	eye_tex_t = tex_y;
	eye_tex_b = tex_y + tex_height;
}

enum Direction reverse_dir(enum Direction dir)
{
	switch (dir) {
	case U:
		return D;
	case D:
		return U;
	case R:
		return L;
	case L:
		return R;
	case X:
	default:
		return X;
	}
}

enum Direction random_dir(const Sprite& sprite)
{
	Direction new_direction = sprite.direction;
	do {
		switch((rand() % 4)) {
		case 0:
			new_direction = U;
			break;
		case 1:
			new_direction = D;
			break;
		case 2:
			new_direction = L;
			break;
		case 3:
			new_direction = R;
			break;
		default:
			new_direction = X;
		}
	} while (hits_wall(sprite, new_direction)
			|| new_direction == X
			|| new_direction == reverse_dir(sprite.direction));
	return new_direction;
}

enum Direction get_dir_from_target(const Sprite& sprite, float x_diff, float y_diff)
{
	Direction ret_dir = X;

	if (fabsf(x_diff) < fabsf(y_diff))
		if (y_diff >= 0 && sprite.direction != D && !hits_wall(sprite, U))
			ret_dir = U;
		else if (sprite.direction != U && !hits_wall(sprite, D))
			ret_dir = D;

	if (ret_dir == X)
		if (x_diff >= 0 && sprite.direction != R && !hits_wall(sprite, L))
			ret_dir = L;
		else if (sprite.direction != L && !hits_wall(sprite, R))
			ret_dir = R;

	if (ret_dir == X)
		if (y_diff >= 0 && sprite.direction != D && !hits_wall(sprite, U))
			ret_dir = U;
		else if (sprite.direction != U && !hits_wall(sprite, D))
			ret_dir = D;

	return ret_dir != X ? ret_dir : random_dir(sprite);
}

enum Direction chase(const Sprite& sprite, const Sprite& pacman, int tile_delta)
{
	float x_diff = sprite.centre(0) - pacman.centre(0);
	float y_diff = sprite.centre(1) - pacman.centre(1);

	switch (pacman.direction) {
	case U:
		y_diff -= tile_delta;
		break;
	case D:
		y_diff += tile_delta;
		break;
	case L:
		x_diff -= tile_delta;
		break;
	case R:
		x_diff += tile_delta;
		break;
	default:
		break;
	}
	return get_dir_from_target(sprite, x_diff, y_diff);
}

enum Direction jitter_chase(const Sprite& sprite, const Sprite& jitter, const Sprite& pacman, int tile_delta)
{
	float x_target = pacman.centre(0);
	float y_target = pacman.centre(1);

	switch (pacman.direction) {
	case U:
		y_target -= tile_delta;
		break;
	case D:
		y_target += tile_delta;
		break;
	case L:
		x_target -= tile_delta;
		break;
	case R:
		x_target += tile_delta;
		break;
	default:
		break;
	}

	x_target += (x_target - jitter.centre(0));
	y_target += (y_target - jitter.centre(1));

	return get_dir_from_target(sprite, sprite.centre(0) - x_target, sprite.centre(1) - y_target);
}

enum Direction scatter(const Sprite& sprite, float target_x, float target_y)
{
	return get_dir_from_target(sprite, sprite.centre(0) - target_x, sprite.centre(1) - target_y);
}
